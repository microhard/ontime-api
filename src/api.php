<?php

define("CONFIG", parse_ini_file("config.ini", true));

define("METHOD", $_SERVER["REQUEST_METHOD"]);
define("ENDPOINT", $_SERVER["REDIRECT_URL"]);
const ACTIONS = array(

	"GET" => array(
		"/state" => "getState",
		"/history" => "getHistory",
		"/user" => "getUser",
		"/clocked" => "getClockedUsers"
	),

	"POST" => array(
		"/mark" => "postMarking"
	)

);

include "endpoints.php";

function errorNotFound() {
	http_response_code(404);
	return "nooo :(";
}

$ACTION = ACTIONS[METHOD][ENDPOINT] or "errorNotFound";
$result = $ACTION();
die(json_encode($result));
