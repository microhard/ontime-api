<?php

require "lib/Database.php";
$DB = new Database();

// GET /state?user_token=:token
// returns 0 if not working currently or n which is the amount of seconds since clock-in
function getState() {
	global $DB;

	if (!isset($_GET["user_token"]))
		http_response_code(400) and die("AAA");
	$user_token = $_GET["user_token"];
	$user_token = $DB->escape($user_token);

	$last = $DB->query("
		SELECT `type`, `timestamp`
		FROM timecard
		WHERE user_token = '$user_token'
		ORDER BY `timestamp`, `id` DESC
		LIMIT 1
	");

	if (!$last or $last[0]["type"] === "OUT")
		return 0;

	return time() - strtotime($last[0]["timestamp"]);
}


// GET /history?user_token=:token
// Returns the previous markings for this user
function getHistory() {
	global $DB;

	if (!isset($_GET["user_token"]))
		http_response_code(400) and die();
	$user_token = $DB->escape($_GET["user_token"]);

	$limit = (int) $_GET["limit"] ?: 10;
	$markings = $DB->query("
		SELECT `type`, `timestamp`
		FROM timecard
		WHERE user_token = '$user_token'
		LIMIT $limit
	");

	return $markings;
}


// POST /mark
// Body: user_token
function postMarking() {
	global $DB;

	if (isset($_POST["user_token"])) {
		$user_token = $_POST["user_token"];
		$user_token = $DB->escape($user_token);
		$user = $DB->query("SELECT token, name, card, account_id FROM user WHERE token = '$user_token'");
		if (!$user)
			http_response_code(418) and die();
		$account_id = $user[0]["account_id"];
		unset($user[0]["account_id"]);
	} elseif (isset($_POST["account_id"]) and isset($_POST["card"])) {
		$account_id = $DB->escape($_POST["account_id"]);
		$card = $DB->escape($_POST["card"]);
		$user = $DB->query("SELECT token, name, card FROM user WHERE account_id = '$account_id' AND card = '$card'");
		if (!$user)
			http_response_code(418) and die();
		$user_token = $user[0]["token"];
	} else
		http_response_code(400) and die();

	$DB->query("CALL mark('$user_token')");

	$webhooks = $DB->query("SELECT * FROM webhook WHERE account_id = '$account_id'");
	if ($webhooks) {
		$data = array();
		$endpoint = $webhook["endpoint"];
		$data["user"] = $user[0];
		$data["timecard"] = $DB->query(
			"SELECT type, timestamp 
			FROM timecard 
			WHERE id = LAST_INSERT_ID()"
		)[0];
		$type = "clock-".strtolower($data["timecard"]["type"]);
		$data = json_encode($data);
		$opts = array("http" => array(
			"method"  => "POST",
			"header"  => "Content-Type: application/json\r\nContent-Length: ".strlen($data)."\r\n",
			"content" => $data
		));
		$context = stream_context_create($opts);
		foreach ($webhooks as $webhook) if ($webhook["when"] === $type) {
			$fp = fopen($webhook["endpoint"], "r", false, $context);
		}
	}

	http_response_code(201);
	return $user[0];
}


// GET /user?(token||card&account_id)
// Returns the user specified by the token or the card and account
function getUser() {
	global $DB;

	if (isset($_GET["token"])) {
		$token = $DB->escape($_GET["token"]);
		$user = $DB->query("SELECT * FROM user WHERE token = '$token'");
		if (!$user)
			http_response_code(404) and die();
		$user = $user[0];
	} elseif (isset($_GET["card"]) and isset($_GET["account_id"])) {
		$card = $DB->escape($_GET["card"]);
		$account_id = $DB->escape($_GET["account_id"]);
		$user = $DB->query("SELECT * FROM user WHERE card = '$card' AND account_id = '$account_id'");
	} else http_response_code(400) and die();

	return $user;
}

function getClockedUsers() {
	global $DB;

	if (!isset($_GET["account_id"]))
		http_response_code(400) and die();
	$account_id = $DB->escape($_GET["account_id"]);

	$clocked_users = $DB->query("
		SELECT u.token, u.name, u.email, u.card
            FROM (timecard t, user u)
            INNER JOIN (
                    SELECT MAX(id) id
                        FROM timecard
                        GROUP BY user_token
                ) AS l
                ON t.id = l.id
            WHERE t.type = 'IN'
            AND u.account_id = '$account_id'
            AND u.token = t.user_token
	");

	return $clocked_users;
}
