<?php

class Database {

	private $conn;

	function __construct() {
		$this->conn = new mysqli(
			CONFIG["database"]["hostname"],
			CONFIG["database"]["username"],
			CONFIG["database"]["password"],
			CONFIG["database"]["database"]
		);
	}

	function escape($string) {
		return $this->conn->real_escape_string($string);
	}

	function query($sql) {
		$result = $this->conn->query($sql);
		echo $this->conn->error;
		if (!$result) return false;
		if (is_object($result) and $result->num_rows > 0) {
			$rows = array();
			while ($row = $result->fetch_assoc())
				array_push($rows, $row);
			return $rows;
		}
		return null;
	}

	function lastid() {
		return $this->conn->insert_id;
	}

}
